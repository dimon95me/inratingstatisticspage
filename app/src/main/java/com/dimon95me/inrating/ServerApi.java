package com.dimon95me.inrating;

import com.dimon95me.inrating.model.Post;
import com.dimon95me.inrating.model.Response;
import com.dimon95me.inrating.model.User;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ServerApi {

    @POST("get")
    Call<Post> getPostInformation(@Query("id") String id);

    @POST("likers/all")
    Call<Response> getLikers(@Query("id") String id);

    @POST("reposters/all")
    Call<Response> getReposters(@Query("id") String id);

    @POST("commentators/all")
    Call<Response> getCommentators(@Query("id") String id);

    @POST("mentions/all")
    Call<Response> getMentions(@Query("id") String id);
}
