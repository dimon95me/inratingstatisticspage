package com.dimon95me.inrating.statistics;

import com.dimon95me.inrating.model.Post;
import com.dimon95me.inrating.model.Response;

public interface StatisticsView {

    void onSuccessGetPostInformation(Post post);

    void onSuccessLikers(Response body);

    void onSuccessReporters(Response body);

    void onSuccessCommentators(Response body);

    void onSuccessMentions(Response body);

    void onRequestFailed(Throwable t);
}
