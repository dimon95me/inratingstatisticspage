package com.dimon95me.inrating.statistics;

import com.dimon95me.inrating.ApiClient;
import com.dimon95me.inrating.ServerApi;
import com.dimon95me.inrating.model.Post;
import com.dimon95me.inrating.model.Response;

import retrofit2.Call;
import retrofit2.Callback;

public class StatisticsPresenter {

    private StatisticsView view;
    private ServerApi api;

    public StatisticsPresenter(StatisticsView view) {
        this.view = view;
        api = ApiClient.getApiClient().create(ServerApi.class);
    }

    void getPostInformation(String id) {
        Call<Post> postInformation = api.getPostInformation(id);
        postInformation.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, retrofit2.Response<Post> response) {
                view.onSuccessGetPostInformation(response.body());
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                view.onRequestFailed(t);
            }
        });
    }

    void getLikers(String id) {
        Call<Response> likers = api.getLikers(id);

        likers.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
            view.onSuccessLikers(response.body());
            response.body().toString();
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                view.onRequestFailed(t);
            }
        });
    }

    void getCommentators(String id) {
        Call<Response> commentators = api.getCommentators(id);
        commentators.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                view.onSuccessCommentators(response.body());
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                view.onRequestFailed(t);
            }
        });
    }

    void getReporters(String id) {
        Call<Response> reposters = api.getReposters(id);
        reposters.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                view.onSuccessReporters(response.body());
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                view.onRequestFailed(t);
            }
        });
    }

    void getMentions(String id) {
        Call<Response> mentions = api.getMentions(id);
        mentions.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                view.onSuccessMentions(response.body());
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                view.onRequestFailed(t);
            }
        });
    }
}
