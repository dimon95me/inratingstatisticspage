package com.dimon95me.inrating.statistics;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dimon95me.inrating.R;
import com.dimon95me.inrating.model.Post;
import com.dimon95me.inrating.model.Response;
import com.dimon95me.inrating.model.User;

import java.util.ArrayList;
import java.util.List;

public class StatisticsActivity extends AppCompatActivity implements StatisticsView {
    private static final String TAG = "StatisticsActivity";

    private StatisticsPresenter presenter;

    private String id;

    private TextView likesTextView, commentsTextView, repostsTextView, otmenkiTextView, bookmarksTextView, viewsTextView;
    private LinearLayout likesLinearLayout, commentsLinearLayout, otmetkiLinearLayout, repostsLinearLayout;

    Glide glide;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        id = "2720";

        initializeViews();


        presenter = new StatisticsPresenter(this);
        presenter.getPostInformation(id);
        presenter.getLikers(id);
        presenter.getCommentators(id);
        presenter.getReporters(id);
        presenter.getMentions(id);
    }

    private void initializeViews() {
        likesTextView = findViewById(R.id.activity_statistics_likes_text_view);
        commentsTextView = findViewById(R.id.activity_statistics_comments_text_view);
        repostsTextView = findViewById(R.id.activity_statistics_reposts_text_view);
        otmenkiTextView = findViewById(R.id.activity_statistics_otmetki_text_view);
        bookmarksTextView = findViewById(R.id.activity_statistics_bookmarks_text_view);
        viewsTextView = findViewById(R.id.activity_statistics_viewcount_textview);

        likesLinearLayout = findViewById(R.id.activity_statistics_likes_linear_layout);
        commentsLinearLayout = findViewById(R.id.activity_statistics_comments_linear_layout);
        repostsLinearLayout = findViewById(R.id.activity_statistics_reposts_linear_layout);
        otmetkiLinearLayout = findViewById(R.id.activity_statistics_otmentki_linear_layout);
    }

    @Override
    public void onSuccessGetPostInformation(Post post) {
        viewsTextView.setText("Просмотры " + post.getViewsCount());
        bookmarksTextView.setText("Закладки " + post.getBookmarksCount());
    }

    @Override
    public void onSuccessLikers(Response body) {
        likesTextView.setText("Лайки " + body.getMeta().getTotal());
        addImageViewsFromResponse(body, likesLinearLayout);
    }

    @Override
    public void onSuccessReporters(Response body) {
        repostsTextView.setText("Репосты " + body.getMeta().getTotal());
        addImageViewsFromResponse(body, repostsLinearLayout);
    }

    @Override
    public void onSuccessCommentators(Response body) {
        commentsTextView.setText("Комментаторы " + body.getMeta().getTotal());
        addImageViewsFromResponse(body, commentsLinearLayout);
    }

    @Override
    public void onSuccessMentions(Response body) {
        otmenkiTextView.setText("Отметки " + body.getMeta().getTotal());
        addImageViewsFromResponse(body, otmetkiLinearLayout);
    }

    @Override
    public void onRequestFailed(Throwable t) {
        Log.e(TAG, "onRequestFailed: retrofitError", t);
    }

    private void addImageViewsFromResponse(Response body, LinearLayout linearLayout) {
        for (LinearLayout temporaryLayout : createListFromServerData(body)) {
            linearLayout.addView(temporaryLayout);
        }
    }

    private List<LinearLayout> createListFromServerData(Response body) {

        List<LinearLayout> temporaryListView = new ArrayList<>();

        for (User user : body.getUsers()) {
            ImageView imageView = new ImageView(this);
            LinearLayout linearLayout = new LinearLayout(this);
            TextView textView = new TextView(this);

            linearLayout.setOrientation(LinearLayout.VERTICAL);

            Glide.with(this).load(user.getAvatarImage().getUrlSmall()).into(imageView);
            imageView.setMinimumWidth(300);
            imageView.setMinimumHeight(300);
            imageView.setPadding(5,5,5,5);

            textView.setText(user.getNickname());
            textView.setPadding(5,0,5,5);
            textView.setGravity(Gravity.CENTER_HORIZONTAL);

            linearLayout.addView(imageView);
            linearLayout.addView(textView);

            temporaryListView.add(linearLayout);
        }

        return temporaryListView;
    }
}
