package com.dimon95me.inrating.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {
    @SerializedName("data")
    @Expose
    private List<User> users;

    @SerializedName("meta")
    @Expose
    private Meta meta;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    @Override
    public String toString() {
        return "Response{" +
                "users=" + users +
                ", meta=" + meta +
                '}';
    }
}
